import 'react-native'
import React from 'react'
import LoginButton from '../../App/Components/LoginButton'
import renderer from 'react-test-renderer'

test('AlertMessage component renders correctly if show is true', () => {
    const tree = renderer.create(<LoginButton />).toJSON()
    expect(tree).toMatchSnapshot()
})

test('AlertMessage component does not render if show is false', () => {
    const tree = renderer.create(<LoginButton show={false} />).toJSON()
    expect(tree).toMatchSnapshot()
})
