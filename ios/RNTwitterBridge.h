//
//  RNTwitterBridge.h
//  Aperto
//
//  Created by Anantha Bhat on 13/07/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <Foundation/Foundation.h>

@interface RNTwitterBridge : NSObject <RCTBridgeModule>
  
  @end

