// @flow
import React, { Component } from 'react'

const twitterItemNavigate = (screenName: string) => WrappedComponent => {
    class TwitterItemNavigate extends Component {
        onPress = (itemDetail) =>{
            this.props.navigation.navigate(screenName, { data: itemDetail })
        }
        render() {
            return <WrappedComponent
                onPress={this.onPress}
                {...this.state}
                {...this.props} />
        }
    }
    return TwitterItemNavigate
}
export default twitterItemNavigate
