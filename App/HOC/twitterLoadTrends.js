import React, { Component } from 'react'
import { connect } from 'react-redux'
import TrendsActions from '../Redux/TrendsRedux'

const twitterLoadTrends = () => WrappedComponent => {
    class TwitterLoadTrends extends Component {
        state = { trends: null }
        dataDownloaded = false
        componentDidMount() {
            const {auth, getTrends } = this.props
            if (!this.dataDownloaded
              && auth.loggedIn) {
                getTrends()
            }
        }
        componentDidUpdate(prevProps) {
            const {startup, trends, auth, getTrends, geo } = this.props
            if (startup.active !== prevProps.startup.active
              && auth.loggedIn) {
                getTrends()
            }
            if(startup.active &&
              (prevProps.geo.geoPoint !== geo.geoPoint )){
                getTrends()
            }
            if (trends.trends !== prevProps.trends.trends) {
                this.setState({ trends: trends.trends })
                this.dataDownloaded = true
            }
        }

        render() {
            return <WrappedComponent {...this.state} {...this.props} />
        }
    }

    const mapStateToProps = state => ({
        trends: state.trends,
        auth: state.auth,
        startup: state.startup,
        geo: state.geo
    })

    const mapDispatchToProps = dispatch => ({
        getTrends: () => dispatch(TrendsActions.getTrends())
    })

    return connect(
        mapStateToProps,
        mapDispatchToProps
    )(TwitterLoadTrends)
}

export default twitterLoadTrends
