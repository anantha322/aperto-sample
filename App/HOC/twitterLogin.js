import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginActions from '../Redux/LoginRedux'
import GeoActions from '../Redux/GeoRedux'

const twitterLogin = () => WrappedComponent => {
    class TwitterLogin extends Component {
        isFirstTimeLoggedIn = true
        checkIfNotLoggedIn = () =>{
            const {startup, auth, twitterLogInPress} = this.props
            if (startup.active && !auth.loggedIn && this.isFirstTimeLoggedIn) {
                twitterLogInPress()
                this.isFirstTimeLoggedIn = false
            }
        }
        getGeoPointIfLoggedIn = (prevProps) =>{
            const {startup, auth, getGeoPoint} = this.props

            if (startup.active && auth.loggedIn &&
               (prevProps.auth.loggedIn !== auth.loggedIn)) {
                getGeoPoint()
            }
        }
        componentDidMount(){
            this.checkIfNotLoggedIn()
        }
        componentDidUpdate(prevProps) {
            this.checkIfNotLoggedIn()
            this.getGeoPointIfLoggedIn(prevProps)
            this.flag = false
        }
        render() {
            return <WrappedComponent
                {...this.state}
                {...this.props} />
        }
    }
    const mapStateToProps = state => ({
        auth: state.auth,
        startup: state.startup,
        geo: state.geo

    })
    const mapDispatchToProps = dispatch => ({
        twitterLogInPress: () => dispatch(LoginActions.login()),
        getGeoPoint: () => dispatch(GeoActions.getGeoPoint())
    })
    return connect(
        mapStateToProps,
        mapDispatchToProps
    )(TwitterLogin)
}
export default twitterLogin
