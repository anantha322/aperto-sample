import React, { Component } from 'react'
import { connect } from 'react-redux'
import GeoActions from '../Redux/GeoRedux'

const twitterSearchPlace = () => WrappedComponent => {
    class TwitterSearchPlace extends Component {
        state = { trends: null }
        dataDownloaded = false
        componentDidMount() {
            const {auth,getTrends} = this.props
            if (!this.dataDownloaded && auth.loggedIn) {
                getTrends()
            }
        }

        render() {
            return <WrappedComponent {...this.state} {...this.props} />
        }
    }

    const mapStateToProps = state => ({
        trends: state.trends,
        geo: state.geo,
        auth: state.auth
    })

    const mapDispatchToProps = dispatch => ({getLocation: name => dispatch(GeoActions.getLocation(name))})

    return connect(
        mapStateToProps,
        mapDispatchToProps
    )(TwitterSearchPlace)
}

export default twitterSearchPlace
