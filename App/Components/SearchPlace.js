import React from 'react'
import { compose } from 'redux'
import Search from 'react-native-search-box'

//HOC
import twitterSearchPlace from '../HOC/twitterSearchPlace'

const searchPlace = ({auth,getLocation}) =>{
    const onSearch = (searchString) => getLocation(searchString)
    if(auth.loggedIn){
        return <Search
            placeholder={'Enter Place or City name'}
            onSearch={onSearch}
            placeholderCollapsedMargin={70}
            searchIconCollapsedMargin={90}
        />
    }
    return null
}
export default compose(
    twitterSearchPlace()
)(searchPlace)
