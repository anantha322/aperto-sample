import React from 'react'
import { View, Text } from 'react-native'
import { SocialIcon } from 'react-native-elements'
import { compose } from 'redux'

//HOC
import twitterLogin from '../HOC/twitterLogin'

//styles
import styles from './Styles/LoginStyles'

const loginButton = ({auth, twitterLogInPress}) =>{
    const showError = () => {
        if (auth.error && auth.error.errorMessage) {
            return <Text style={styles.error}>{error.errorMessage}</Text>
        }
        return null
    }

    if (auth.loggedIn) {
        return null
    }
    return (
        <View style={styles.container}>
            <SocialIcon
                onPress={twitterLogInPress}
                title="Login With Twitter"
                button
                type="twitter"
                style={styles.socialBtn}
            />
            {showError()}
        </View>
    )

}
export default compose(
    twitterLogin()
)(loginButton)
