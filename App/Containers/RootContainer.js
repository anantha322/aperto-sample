import React, { Component } from 'react'
import { View, StatusBar, YellowBox } from 'react-native'
import Navigation from '../Navigation/AppNavigation'
import { connect } from 'react-redux'
import ReduxPersist from '../Config/ReduxPersist'
import StartupActions from '../Redux/StartupRedux'
import LoginActions from '../Redux/LoginRedux'
import DebugConfig from '../Config/DebugConfig'
// Styles
import styles from './Styles/RootContainerStyles'

class RootContainer extends Component {
    constructor(props) {
        super(props)
        YellowBox.ignoreWarnings(DebugConfig.warningReactNative)
    }
    componentDidMount() {
        if (!ReduxPersist.active) {
            this.props.startup()
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.start.active !== prevProps.start.active) {
            // Initialize twitter
            this.props.twitterConfig()
        }
    }

    render() {
        return (
            <View style={styles.applicationView}>
                <StatusBar barStyle="dark-content" />
                <Navigation />
            </View>
        )
    }
}

const mapStateToProps = state => ({
    start: state.startup
})
const mapDispatchToProps = dispatch => ({
    startup: () => dispatch(StartupActions.startup()),
    twitterConfig: () => dispatch(LoginActions.twitterConfig())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RootContainer)
