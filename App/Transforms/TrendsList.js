// @flow

import type { TrendsFromList } from '../types'

export const normalizeTrendsState = (trends: Array<TrendsFromList>) => {
    let byId = {}
    trends.forEach((trend, index) => {
        byId[index] = trend
    })
    return {
        byId,
        allIds: Object.keys(byId)
    }
}
