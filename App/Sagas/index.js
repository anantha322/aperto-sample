import { takeLatest, all } from 'redux-saga/effects'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GeoTypes } from '../Redux/GeoRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { TrendsTypes } from '../Redux/TrendsRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getGeoPoint, getLocation } from './GeoSagas'
import { getTrends } from './TrendsSagas'
import { login, twitterConfig } from './LoginSagas'

/* ------------- API ------------- */

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
    yield all([
        // some sagas only receive an action
        takeLatest(StartupTypes.STARTUP, startup),
        takeLatest(TrendsTypes.GET_TRENDS, getTrends),
        takeLatest(GeoTypes.GET_GEO_POINT, getGeoPoint),
        takeLatest(LoginTypes.TWITTER_CONFIG, twitterConfig),
        takeLatest(LoginTypes.LOGIN, login),
        takeLatest(GeoTypes.GET_LOCATION, getLocation)
    ])
}
