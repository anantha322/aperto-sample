import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import { AsyncStorage } from 'react-native'

export default {
    active: true,
    reducerVersion: '1.0',
    storeConfig: {
        key: 'aperto1',
        storage: AsyncStorage,
        blacklist: ['nav', 'startup'],
        transforms: [immutablePersistenceTransform]
    }
}
