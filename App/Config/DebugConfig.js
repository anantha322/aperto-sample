// some of dependency lib are not updated to latest version of RN, which may encounter deprecated warnings
const warningReactNative=['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']
export default {
    useFixtures: false,
    yellowBox: __DEV__,
    reduxLogging: __DEV__,
    warningReactNative: warningReactNative
}
