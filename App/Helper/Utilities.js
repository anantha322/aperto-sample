import GeoCoder from 'react-native-geocoder'
// TODO : move geoCoderKey to env file
const geoCoderKey = 'AIzaSyB6Tg04OcyHpXt0lJfUFAlIGaRHwsc9O0w'
GeoCoder.fallbackToGoogle(geoCoderKey)

const error = {
    errorCode: 'ERROR',
    errorMessage: 'Geolocation not supported'
}
export default {
    watchPosition: () => {
        if (navigator.geolocation) {
            return new Promise((resolve, reject) =>
                navigator.geolocation.getCurrentPosition(resolve, reject, {
                    enableHighAccuracy: true
                })
            )
        }
        return new Promise(resolve => resolve({ error }))
    },
    createURLForWoeid: (baseURL: string, parameter: Object | string) => {
        let query = ''
        if (typeof parameter !== 'string') {
            query = `?q=SELECT woeid FROM geo.places WHERE text=\"(${parameter.longitude},${
                parameter.latitude
            })\"&format=json`
        } else {
            query = `?q=SELECT woeid FROM geo.places WHERE text=\"(${parameter})\"&format=json`
        }
        return baseURL + query
    },
    getLocationData: (locationName: string) => GeoCoder.geocodeAddress(locationName),
    getLocationByCoordinates: (coordinates: Object) => GeoCoder.geocodePosition(coordinates)
}
