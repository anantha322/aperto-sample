import { createStackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import TrendDetailsScreen from '../Containers/TrendDetailsScreen'

import styles from './Styles/NavigationStyles'
import { Colors } from '../Themes/'

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
    {
        LaunchScreen: {
            screen: LaunchScreen,
            navigationOptions: () => ({ title: 'Twitter Trends' })
        },
        TrendDetail: {
            screen: TrendDetailsScreen,
            navigationOptions: () => ({ title: 'Trend Detail' })
        }
    },
    {
        // Default config for all screens
        initialRouteName: 'LaunchScreen',
        navigationOptions: {
            headerStyle: styles.header,
            headerTintColor: Colors.snow
        }
    }
)

export default PrimaryNav
