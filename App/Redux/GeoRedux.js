import { createActions, createReducer } from 'reduxsauce'
import type { UserCoordinates, ErrorTypes } from '../Types'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    getGeoPoint: null,
    saveGeoPoint: ['geoPoint'],
    errorGeoPoint: ['error'],
    saveGeoId: ['geoId'],
    getLocation: ['location']
})

export const GeoTypes = Types
export default Creators

type State = {
    geoPoint: UserCoordinates,
    error: ErrorTypes,
    geoId: number
}

export const INITIAL_STATE: State = {
    geoPoint: {},
    geoId: 1,
    error: {
        errorCode: '',
        errorMessage: ''
    }
}

export const saveGeoPoint = (state: State, { geoPoint }: { geoPoint: Array<UserCoordinates> }) =>
    ({
        ...state,
        geoPoint: geoPoint
    }: State)
export const errorGeoPoint = (state: State, { error }: { error: Object<ErrorTypes> }) =>
    ({
        ...state,
        error: error
    }: State)

export const saveGeoId = (state: State, { geoId }: { geoId: number }) => ({
    ...state,
    geoId: geoId
})

export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_GEO_POINT]: null,
    [Types.SAVE_GEO_POINT]: saveGeoPoint,
    [Types.ERROR_GEO_POINT]: errorGeoPoint,
    [Types.SAVE_GEO_ID]: saveGeoId,
    [Types.GET_LOCATION]: null
})
