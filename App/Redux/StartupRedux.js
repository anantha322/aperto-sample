import { createActions, createReducer } from 'reduxsauce'
/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    startup: null
})

export const StartupTypes = Types
export default Creators

type State = {
    +active: boolean,
}

export const INITIAL_STATE: State = {
    active: false
}

export const startup = state =>
    ({
        ...state,
        active: true
    }: State)

export const reducer = createReducer(INITIAL_STATE, {
    [Types.STARTUP]: startup
})
