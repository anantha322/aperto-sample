//import all your reducers here
export const startup = require('./StartupRedux').reducer
export const trends = require('./TrendsRedux').reducer
export const auth = require('./LoginRedux').reducer
export const geo = require('./GeoRedux').reducer
